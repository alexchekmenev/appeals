/**
 * Useful links
 *
 * http://docs.sequelizejs.com/manual/tutorial/associations.html#belongs-to-many-associations
 * http://docs.sequelizejs.com/class/lib/associations/base.js~Association.html
 * https://medium.com/@prajramesh93/getting-started-with-node-express-and-mysql-using-sequelize-ed1225afc3e0
 */

"use strict";

const Sequelize = require('sequelize');
const sequelizeImport = require('sequelize-import');
const path = require('path');
const mysqlModelsDir = path.join(__dirname, '../models');

let _sequelize = null;

module.exports.myConnect = (config) => {
    _sequelize = new Sequelize(
        config.get('mysql:database'),
        config.get('mysql:user'),
        config.get('mysql:password'), {
            host: config.get('mysql:host'),
            dialect: 'mysql',
            pool: {
                min: 10,
                max: 100,
                idle: 60000,
                acquire: 10000,
            },
            logging: console.log, // TODO: remove in production,
            timezone: '+00:00' // useful link https://medium.com/@toastui/handling-time-zone-in-javascript-547e67aa842d
        }
    );
    return _sequelize
        .authenticate()
        .then(() => {
            // debug('Connection has been established successfully.');
            return sequelizeImport(mysqlModelsDir, _sequelize, {
                exclude: []
            });
        })
        .then(models => {
            // debug('Models were successfully registered');
            Object.values(models).forEach(model => {
                if (model.hasOwnProperty('associate')) {
                    model.associate(models)
                }
            });
            // return models;
        }).then(_ => {
            return _sequelize.sync({force: true});
        }).then(_ => {
            return _sequelize.models;
        });
};

module.exports.getSequelize = () => {
    return _sequelize;
};

module.exports.myDisconnect = () => {
    if (_sequelize) {
        _sequelize.close();
    }
};

"use strict";

const { getSequelize } = require('../datastore/sequelize');

module.exports = {
    create,
    getById
};

async function create(options) {
    if (!options.name || !options.email || !options.phone) {
        return null;
    }

    const { User } = getSequelize().models;
    const user = {
        name: options.name,
        email: options.email,
        phone: options.phone,
        is_moderator: options.isModerator
    };
    return await User.create(user);
}

async function getById(userId) {
    if (!userId) {
        return null;
    }

    const { User } = getSequelize().models;
    return await User.findOne({
        where: {
            id: +userId
        }
    });
}
"use strict";

const { getSequelize } = require('../datastore/sequelize');

const { translateToRussian, translateToEnglish, getKeywords } = require('./watson');

module.exports = {
    extractKeywordsFromText,
    saveKeyword,
    // prefixMatchSearch,
    getTicketKeywords,
    addKeywordToTicket,
    removeKeywordFromTicket
};

async function extractKeywordsFromText(text) {
    const englishText = await translateToEnglish(text);
    const keywords = await getKeywords(englishText);
    const result = [];
    for (const keyword of keywords) {
        const tmp = await translateToRussian(keyword);
        result.push(tmp);
    }
    return result;
}

async function saveKeyword(keyword) {
    if (!keyword) {
        return null;
    }
    const kw = keyword.toLowerCase();
    const found = await fullMatchSearch(kw);
    if (!found) {
        const { Keyword } = getSequelize().models;
        return await Keyword.create({
            text: kw
        });
    } else {
        return found;
    }
}

async function fullMatchSearch(keyword) {
    if (!keyword) {
        return null;
    }
    const { Keyword } = getSequelize().models;
    const result = await Keyword.findOne({
        where: {
            text: keyword.toLowerCase()
        }
    });
    return result;
}

// async function prefixMatchSearch(prefix) {
//     if (!prefix) {
//         return null;
//     }
//     const sequelize = require('../datastore/sequelize');
//     const { Op } = sequelize.getSequelize();
//     const { Keyword } = sequelize.getSequelize().models;
//     const result = await Keyword.findOne({
//         where: {
//             text: {[Op.like]: prefix.toLowerCase() + '%'}
//         }
//     });
//     return result;
// }

async function getTicketKeywords(ticketId) {
    if (!ticketId) {
        return null;
    }

    const { Ticket, TicketKeyword, Keyword } = getSequelize().models;
    const ticket = await Ticket.findOne({
        where: {
            id: +ticketId
        }
    });
    return ticket.getKeywords();
}

async function addKeywordToTicket(ticketId, keywordId) {
    if (!ticketId || !keywordId) {
        return null;
    }

    const { Ticket, TicketKeyword, Keyword } = getSequelize().models;
    const ticket = await Ticket.findOne({
        where: {
            id: +ticketId
        }
    });
    const keyword = await Keyword.findOne({
        where: {
            id: +keywordId
        }
    });
    return ticket.addKeyword(keyword);

}

async function removeKeywordFromTicket(ticketId, keywordId) {
    if (!ticketId || !keywordId) {
        return null;
    }

    const { Ticket, TicketKeyword, Keyword } = getSequelize().models;
    const ticket = await Ticket.findOne({
        where: {
            id: +ticketId
        }
    });
    const keyword = await Keyword.findOne({
        where: {
            id: +keywordId
        }
    });
    return ticket.removeKeyword(keyword);
}
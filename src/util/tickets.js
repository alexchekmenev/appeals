"use strict";

const sequelize = require('../datastore/sequelize');
const { translateToEnglish, getKeywords } = require('./watson');
const { saveKeyword, extractKeywordsFromText } = require('./keywords');

module.exports = {
    create,
    getById,
    getAll
};

async function create(options) {
    if (!options.userId || !options.text) {
        return null;
    }

    const { Ticket } = sequelize.getSequelize().models;
    const ticket = {
        user_id: options.userId,
        status: 'new',
        text: options.text
    };
    const createdTicket = await Ticket.create(ticket);

    const keywords = await extractKeywordsFromText(ticket.text);
    console.log(keywords);
    const filtered = keywords.filter(t => !!t);

    // const translatedText = await translateToEnglish(ticket.text);
    // const keywords = await getKeywords(translatedText);

    for (const keyword of keywords) {
        const saved = await saveKeyword(keyword);
        createdTicket.addKeyword(saved);
    }
    return createdTicket;
}

async function getById(ticketId) {
    if (!ticketId) {
        return null;
    }

    const { Ticket, User, Category, Executor, Reply } = sequelize.getSequelize().models;
    return await Ticket.findOne({
        where: {
            id: ticketId
        },
        include: [{
            as: 'user',
            model: User
        }, {
            as: 'category',
            model: Category
        }, {
            as: 'executor',
            model: Executor
        }, {
            as: 'reply',
            model: Reply
        }]
    });
}

async function getAll(options) {
    const sequelize = require('../datastore/sequelize');
    const { Op } = sequelize.getSequelize();
    // userId, public, categoryId
    let filter = {};
    // if (options.userId && options.public) {
    //     filter = {$or: [
    //         {user_id: +options.userId},
    //         {is_public: options.public}
    //     ]};
    // } else
    if (options.userId) {
        filter.user_id = +options.userId;
    } else if (options.public) {
        filter.is_public = options.public;
    }
    if (options.categoryId) {
        filter.category_id = +options.categoryId;
    }

    console.log(filter);

    const { Ticket, User } = sequelize.getSequelize().models;
    return await Ticket.findAll({
        where: filter
    });
}


async function getTicketCategory() {
    // TODO: hardcoded mapping
}

async function getTicketExecutor() {
    // TODO: hardcoded mapping
}
"use strict";

const rp = require('request-promise');

module.exports = {
    translateToEnglish,
    translateToRussian,
    getKeywords
};

async function translateToEnglish(text) {
    try {
        const options = {
            method: 'POST',
            url: 'https://gateway-lon.watsonplatform.net/language-translator/api/v3/translate',
            qs: {
                version: '2019-01-01'
            },
            headers: {
                Authorization: 'Basic YXBpa2V5OlcwUkh2Tk5NckNmWEJpQjgxV2JobWxHeXpqRUVFMHB2LTdDeDVrOWJMUWEx',
                'Content-Type': 'application/json'
            },
            body: {"text": text, "model_id":"ru-en"},
            json: true
        };
        const response = await rp(options);

        return (response.translations.length > 0 ? response.translations[0].translation : null);
    } catch (e) {
        console.error(e.message);
    }
}

async function translateToRussian(text) {
    try {
        const options = {
            method: 'POST',
            url: 'https://gateway-lon.watsonplatform.net/language-translator/api/v3/translate',
            qs: {
                version: '2019-01-01'
            },
            headers: {
                Authorization: 'Basic YXBpa2V5OlcwUkh2Tk5NckNmWEJpQjgxV2JobWxHeXpqRUVFMHB2LTdDeDVrOWJMUWEx',
                'Content-Type': 'application/json'
            },
            body: {"text": text, "model_id":"en-ru"},
            json: true
        };
        const response = await rp(options);

        return (response.translations.length > 0 ? response.translations[0].translation : null);
    } catch (e) {
        console.error(e.message);
    }
}

async function getKeywords(text) {
    try {
        const options = {
            method: 'POST',
            url: 'https://gateway-lon.watsonplatform.net/natural-language-understanding/api/v1/analyze',
            qs: {
                version: '2019-01-01'
            },
            headers: {
                Authorization: 'Basic YXBpa2V5OlozZzREUlBzdmhUSWZ2OFVONEl2ZFItOUhlMEVvQnNxMU1keFlFVzZQOXNN',
                'Content-Type': 'application/json'
            },
            body: {
                text: text,
                features: {keywords: {emotion: true}},
            },
            json: true
        };
        const response = await rp(options);

        // TODO: фильтруем ключевые слова перед тем как добавлять в тикет
        const keywords = (response.keywords ? response.keywords.filter(keyword => {
            return keyword.relevance > 0.6;
        }) : []).map(keyword => keyword.text);
        return keywords.length > 0 ? keywords : [];
    } catch (e) {
        console.error(e.message);
    }


}
'use strict';

const sequelize = require('./datastore/sequelize');

const { translateToRussian } = require('./util/watson');

module.exports = async (config) => {
    try {
        await sequelize.myConnect(config);
    } catch (e) {
        console.error(e.message);
    }
};
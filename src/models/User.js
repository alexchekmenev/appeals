'use strict';

module.exports = function (sequelize, DataTypes) {
    const User = sequelize.define('User', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
            required: true
        },
        email: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        phone: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        is_moderator: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
    }, {
        underscored: true,
        timestamps: false,
        tableName: 'user'
    });

    User.associate = (models) => {
        const { User, Ticket } = models;

        User.hasMany(Ticket, {
            as: 'tickets',
            foreignKey: 'user_id',
            sourceKey: 'id'
        });
    };

    return User;
};
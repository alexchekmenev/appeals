'use strict';

module.exports = function (sequelize, DataTypes) {
    const TicketKeyword = sequelize.define('TicketKeyword', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        ticket_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            required: true
        },
        keyword_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            required: true
        }
    }, {
        underscored: true,
        timestamps: false,
        tableName: 'ticket_keyword'
    });

    return TicketKeyword;
};
'use strict';

module.exports = function (sequelize, DataTypes) {
    const Executor = sequelize.define('Executor', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        type: {
            type: DataTypes.STRING(255),
            allowNull: false,
            required: true
        },
        person_name: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        department_name: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        organisation_name: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
        email: {
            type: DataTypes.STRING(255),
            allowNull: true
        },
    }, {
        underscored: true,
        timestamps: false,
        tableName: 'executor'
    });

    Executor.associate = (models) => {
        const { Executor, Ticket, Reply } = models;

        Executor.hasMany(Ticket, {
            as: 'tickets',
            foreignKey: 'executor_id',
            sourceKey: 'id'
        });

        Executor.hasMany(Reply, {
            as: 'replies',
            foreignKey: 'executor_id',
            sourceKey: 'id'
        });
    };

    return Executor;
};
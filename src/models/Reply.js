'use strict';

module.exports = function (sequelize, DataTypes) {
    const Reply = sequelize.define('Reply', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        text: {
            type: DataTypes.TEXT,
            allowNull: false,
            required: true
        },
    }, {
        underscored: true,
        timestamps: false,
        tableName: 'reply'
    });

    Reply.associate = (models) => {
        const { Reply, Executor } = models;

        Reply.belongsTo(Executor, {
            as: 'executor',
            foreignKey: 'executor_id',
            targetKey: 'id'
        });
    };

    return Reply;
};
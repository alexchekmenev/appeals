'use strict';

module.exports = function (sequelize, DataTypes) {
    const Category = sequelize.define('Category', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING(255),
            allowNull: false,
            required: true
        }
    }, {
        underscored: true,
        timestamps: false,
        tableName: 'category'
    });

    Category.associate = (models) => {
        const { Category, Ticket } = models;

        Category.hasMany(Ticket, {
            as: 'tickets',
            foreignKey: 'category_id',
            sourceKey: 'id'
        });
    };

    return Category;
};
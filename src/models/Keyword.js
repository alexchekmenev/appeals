'use strict';

module.exports = function (sequelize, DataTypes) {
    const Keyword = sequelize.define('Keyword', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        text: {
            type: DataTypes.STRING(255),
            allowNull: false,
            required: true
        },
        // approved: {
        //     type: DataTypes.BOOLEAN,
        //     defaultValue: false
        // },
    }, {
        underscored: true,
        timestamps: false,
        tableName: 'keyword'
    });

    Keyword.associate = (models) => {
        const { Keyword, Ticket, TicketKeyword } = models;

        Keyword.belongsToMany(Ticket, {
            as: 'tickets',
            through: TicketKeyword,
            foreignKey: 'keyword_id',
            otherKey: 'ticket_id'
        });
    };

    return Keyword;
};
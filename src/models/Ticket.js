'use strict';

module.exports = function (sequelize, DataTypes) {
    const Ticket = sequelize.define('Ticket', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: {
            type: DataTypes.STRING(255),
            allowNull: false,
            required: true
        },
        text: {
            type: DataTypes.TEXT,
            allowNull: false,
            required: true
        },
        is_public: {
            type: DataTypes.BOOLEAN,
            defaultValue: false
        },
    }, {
        underscored: true,
        timestamps: true,
        tableName: 'ticket'
    });

    Ticket.associate = (models) => {
        const { Ticket, User, Keyword, Category, Executor, Reply, TicketKeyword } = models;

        Ticket.belongsTo(User, {
            as: 'user',
            foreignKey: 'user_id',
            targetKey: 'id'
        });

        Ticket.belongsTo(Executor, {
            as: 'executor',
            foreignKey: 'executor_id',
            targetKey: 'id'
        });

        Ticket.belongsTo(Reply, {
            as: 'reply',
            foreignKey: 'reply_id',
            targetKey: 'id'
        });

        Ticket.belongsTo(Category, {
            as: 'category',
            foreignKey: 'category_id',
            targetKey: 'id'
        });

        Ticket.belongsToMany(Keyword, {
            as: 'keywords',
            through: TicketKeyword,
            foreignKey: 'ticket_id',
            otherKey: 'keyword_id'
        });
    };

    return Ticket;
};
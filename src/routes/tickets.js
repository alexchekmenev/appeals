"use strict";

const express = require('express');
const router = express.Router();
const { getById, create, getAll } = require('../util/tickets');
const { addKeywordToTicket, removeKeywordFromTicket, getTicketKeywords } = require('../util/keywords');

router.get('/:ticket_id', async function(req, res, next) {
    const ticketId = req.params.ticket_id || null;
    if (ticketId) {
        res.json(await getById(ticketId));
    } else {
        res.status(400).send('Ticket not found');
    }
});

router.get('/', async function(req, res, next) {
    const options = {
        userId: req.query.user_id || null,
        public: parseInt(req.query.public) || 0,
        categoryId: req.query.category_id || null,
    };
    res.json(await getAll(options));
});

router.post('/', async function (req, res, next) {
    const options = {
        userId: parseInt(req.query.user_id) || null,
        text: req.body.text || null
    };

    if (!options.userId || !options.text) {
        res.status(400).send('Wrong ticket params');
    } else {
        const user = await create(options);
        res.status(201).json(user);
    }
});

router.get('/:ticket_id/keywords', async function (req, res, next) {
    const options = {
        ticketId: parseInt(req.params.ticket_id) || null
    };

    if (!options.ticketId) {
        res.status(400).send('Wrong ticketId');
    } else {
        const keywords = await getTicketKeywords(options.ticketId);
        res.status(200).json(keywords);
    }
});

router.put('/:ticket_id/keywords', async function (req, res, next) {
    const options = {
        ticketId: parseInt(req.params.ticket_id) || null,
        keywordId: parseInt(req.body.keyword_id) || null
    };

    if (!options.ticketId || !options.keywordId) {
        res.status(400).send('Wrong ticketId or keywordId');
    } else {
        await addKeywordToTicket(options.ticketId, options.keywordId);
        res.status(200).end();
    }
});

router.delete('/:ticket_id/keywords', async function (req, res, next) {
    const options = {
        ticketId: parseInt(req.params.ticket_id) || null,
        keywordId: parseInt(req.body.keyword_id) || null
    };

    if (!options.ticketId || !options.keywordId) {
        res.status(400).send('Wrong ticketId or keywordId');
    } else {
        await removeKeywordFromTicket(options.ticketId, options.keywordId);
        res.status(200).end();
    }
});

module.exports = router;

"use strict";

const express = require('express');
const router = express.Router();
const ph = require('phone');
const { getById, create } = require('../util/users');

router.get('/:user_id', async function(req, res, next) {
    const userId = req.params.user_id || null;
    if (userId) {
        res.json(await getById(userId));
    } else {
        res.status(400).send('User not found');
    }
});

router.post('/', async function (req, res, next) {
    const options = {
        name: req.body.name || null,
        email: req.body.email || null,
        phone: req.body.phone || null,
        isModerator: parseInt(req.body.moderator) || 0
    };

    const phoneResponse = options.phone ? ph(options.phone, 'RU') : null;
    if (phoneResponse && phoneResponse[0] && phoneResponse[0].length > 0) {
        options.phone = phoneResponse[0];
    } else {
        options.phone = null;
    }

    if (!options.name || !options.email || !options.phone) {
        res.status(400).send('Wrong user params');
    } else {
        const user = await create(options);
        res.status(201).json(user);
    }
});

module.exports = router;

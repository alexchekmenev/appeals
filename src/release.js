'use strict';

const sequelize = require('./datastore/sequelize');

module.exports = async (config) => {
    try {
        await sequelize.myDisconnect();
    } catch (e) {
        console.error(e.message);
    }
};
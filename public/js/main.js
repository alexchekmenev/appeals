// $('.ui.form')
//     .form({
//         fields: {
//             name     : 'empty',
//             gender   : 'empty',
//             username : 'empty',
//             password : ['minLength[6]', 'empty'],
//             skills   : ['minCount[2]', 'empty'],
//             terms    : 'checked'
//         }
//     })
// ;

(function ($) {

    $(document).ready(function () {
        render();
    });

    function render() {
        var qs = window.location.search.split('?')[1];
        var ticketId = qs ? parseInt(qs.split('=')[1]) : null;

        if (ticketId) {
            $.ajax({
                url: '/tickets/' + ticketId,
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    renderTicket(ticketId, data);
                }
            });
        }
    }

    function renderTicket(ticketId, data) {
        console.log('rrrr');
        var id = $("#ticket input[name='id']")[0];
        $(id).val(data.id);
        var text = $("#ticket textarea[name='text']")[0];
        $(text).val(data.text);

        getKeywords(ticketId, function (ticketId, data) {
            console.log(data);
            var keywords = $('#keywords');
            $(keywords).html('');
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var text = data[i].text;
                $(keywords).append(
                    '<a class="ui label">\n' +
                    '    ' + text + ' <i class="delete icon delete-keyword" data-keyword-id="' + id +'"></i>\n' +
                    '</a>');
            }

            $('.delete-keyword').on('click', function (e) {
                var keywordId = parseInt($(this).attr('data-keyword-id'));

                console.log('remove keyword', keywordId);
                removeKeyword(ticketId, keywordId, function () {
                    console.log('cb');
                    render();
                });

                // e.preventDefault();
                // return true;
            })
        });
    }

    function getKeywords(ticketId, cb) {
        $.ajax({
            url: '/tickets/' + ticketId + '/keywords',
            dataType: 'json',
            success: function (data) {
                cb(ticketId, data);
            }
        });
    }

    function removeKeyword(ticketId, keywordId, cb) {
        console.log(arguments);
        $.ajax({
            type: 'DELETE',
            url: '/tickets/' + ticketId + '/keywords',
            dataType: 'json',
            data: {
                keyword_id: keywordId
            },
            success: function (data) {
                console.log('deleted');
                cb();
            },
            error: function (err) {
                cb();
            }
        });

    }

    function getExecutor(ticketId) {
        // $.ajax({
        //     url: '/tickets/' + ticketId + '/executor',
        //     dataType: 'json',
        //     success: function (data) {
        //         console.log(data);
        //     }
        // });
    }

})(jQuery);
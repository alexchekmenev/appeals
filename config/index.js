/**
 * Created by creed on 15.04.17.
 */
'use strict';

const nconf = require('nconf');
const path = require('path');
const debug = require('debug')(global.TAG + ':[config]');

let _initialized = null;

module.exports = {
    init: (environment) => {

        if (!environment ||
            environment.indexOf('local') !== 0 &&
            environment.indexOf('dev') !== 0 &&
            environment.indexOf('prod') !== 0 &&
            environment.indexOf('test') !== 0) {
            throw new Error('Wrong environment type');
        }
        if (_initialized !== null) {
            throw new Error('Config is already initialized');
        }
        let p;
        if (environment.indexOf('dev') === 0) {
            p = path.join(__dirname, 'development.json');
        } else if (environment.indexOf('test') === 0) {
            p = path.join(__dirname, 'test.json');
        } else if (environment.indexOf('prod') === 0) {
            p = path.join(__dirname, 'production.json');
        } else {
            p = path.join(__dirname, 'local.json');
        }
        nconf.file({file: p});
        debug(`Project deployed in ${environment.toUpperCase()} mode`);
        return _initialized = nconf;
    },
    // getCached: () => {
    //     if (_initialized === null) {
    //         throw new Error('Config is not initialized');
    //     }
    //     return _initialized;
    // }
};